import Link from 'next/link';

export default function Home() {
    return (
        <>
            <noscript>js is disabled</noscript>
            <div>
                <Link href = '/about'>
                    <a>About</a>
                </Link>
&nbsp;
                <Link href = '/profile'>
                    <a>Profile</a>
                </Link>
            </div>
        </>

    );
}
